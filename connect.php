<?php
  //database
  $myServer = "local";
  $myUser = "myUsername";
  $myPass = "myPassword";
  $myDB = "myDatabaseName";

  //user table
  $userTblName = "User";

   $connectionInfo = array("Database"=>$myDB, "UID" => $myUser, "PWD" => $myPass);
   $conn = sqlsrv_connect( $myServer, $connectionInfo);

   if( !$conn ) {
     echo "Connection could not be established.<br />";
     die( print_r( sqlsrv_errors(), true));
   }
   
?>